package com.example.moopiing.geniusnotepad;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MyNote extends AppCompatActivity {

    private TextView name;
    private TextView descrition;

    private Note note;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        Intent intent = getIntent();
        note = (Note)intent.getSerializableExtra("note");
        initComponents();
    }

    private void initComponents() {
        name = (TextView) findViewById(R.id.subject);
        descrition = (TextView) findViewById(R.id.body);
        name.setText(note.getName());
        descrition.setText(note.getDescription());
    }
}
