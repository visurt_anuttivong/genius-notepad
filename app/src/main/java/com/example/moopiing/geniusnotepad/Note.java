package com.example.moopiing.geniusnotepad;

import java.io.Serializable;

public class Note implements Serializable {

    private String name;
    private String description;

    public Note(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return description;
    }

    public String getDescription() {
        return description;
    }
}
