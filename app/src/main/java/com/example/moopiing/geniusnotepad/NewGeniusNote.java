package com.example.moopiing.geniusnotepad;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NewGeniusNote extends AppCompatActivity {

    private TextView name;
    private TextView discription;
    private Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_genius_note);
        initComponents();
    }

    private void initComponents() {
        name = (TextView) findViewById(R.id.name_field);
        discription = (TextView) findViewById(R.id.description_field);
        saveButton = (Button) findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewGeniusNote.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
}